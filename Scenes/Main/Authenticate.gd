extends Node

var network = NetworkedMultiplayerENet.new()
var port = 1911
var max_servers = 5

func _ready():
	StartServer()


func StartServer():
	network.create_server(port, max_servers)
	get_tree().set_network_peer(network)
	print("authentication server started")
	
	network.connect("connection_failed",self,"_Peer_Connected")
	network.connect("connection_succeeded",self,"_Peer_Disconnected")
	
func _Peer_Connected(gateway_id):
	print("Gateway " + str(gateway_id) + " Connected")
	
func _Peer_Disconnected(gateway_id):
	print("Gateway " + str(gateway_id) + " Disconnected")
	
	
remote func AuthenticatePlayer(username, password, player_id):
	print("authentication request recieved")
	var token
	var gateway_id =get_tree().get_rpc_sender_id()
	var result
	print("starting authentication")
	
	if not PlayerData.players.has(username):
		print("User not recognized")
		result = false
	elif not PlayerData.players[username].password  == password:
		print("incorrect password, should be: " + str(PlayerData.players[username].password))
		result = false
	else:
		print("User recognized")
		result = true
		
		randomize()
		var random_number = randi()
		var hashed = str(random_number).sha256_text()
		var timestamp = str(OS.get_unix_time())
		token = hashed + timestamp
		print(token)
		var gameserver = "Eypo"
		GameServers.DistributeLoginToken(token,gameserver)
		
	print("authentication result send to gateway server")
	rpc_id(gateway_id, "AuthenticationResults", result, player_id, token)


remote func CreateAccount(username, password, player_id):
	var gateway_id = get_tree().get_rpc_sender_id()
	var result
	var message
	if PlayerData.players.has(username):
		result = false
		message = 2
	else:
		result = true
		message = 3
		PlayerData.players[username] = {"password": password}
		PlayerData.SavePlayers()
	
	rpc_id(gateway_id, "CreateAccountResults", result, player_id, message)
