extends Node

#var path = "res://Data/Players.json"
var path = "/srv/authsrv/data/users/Accounts.json"

var players


# Called when the node enters the scene tree for the first time.
func _ready():
	var players_file = File.new()
	players_file.open(path, File.READ)
	players = parse_json(players_file.get_as_text())
	players_file.close()

func SavePlayers():
	var save_file = File.new()
	save_file.open(path, File.WRITE)
	if save_file.is_open():
		save_file.store_line(to_json(players))
		save_file.close()
	else:
		print("file not open")
